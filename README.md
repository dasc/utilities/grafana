# Grafana Dashboards

A variety of grafana dashboards are used in the DCS/LDAS clusters.  This
file gives a brief description of each dashboard, as well as information
on the data source that is used by the dashboard.

The setup and configuration of the individual data sources (graphite, prometheus, and influxdb) are beyond the scope of this document.  However, snippets of the relevant data source configuration are sometimes provided.

To load a dashboard into grafana, you will need to log into the grafana service as an admin user and navigate to Dashboards -> Manage. Click the `Import` button, then paste the contents of the dashboard's .json file into the 'Import via panel json' text box.

## ceph

The ceph dashboard monitors the health of a ceph cluster.  It uses the
prometheus data source to scrape data from the ceph manager module.
The entry in prometheus.yml looks like:

```yaml
  - job_name: 'ceph'
    scrape_interval: 30s
    scrape_timeout: 20s
    static_configs:
    - targets: ['ceph1:9283']
    - targets: ['ceph2:9283']
    - targets: ['ceph3:9283']
    - targets: ['ceph4:9283']
```

The ceph manager can be configured to allow data collection by running the
following commands on any ceph monitor/manager:

```
ceph module enable prometheus
ceph config set mgr mgr/prometheus/scrape_interval 15
ceph config set mgr mgr/prometheus/stale_cache_strategy fail
```

## cit_lag

The Lag Plot dashboard shows the data lag as it arrives from the
observatories to CIT.

This dashboard uses a cron job to publish lag data to an influx database.

## condor

The condor dashboard monitors the status of a condor cluster.  The [fifemon condor probe](https://git.ligo.org/michael.thomas/fifemon) runs as a systemd service to query the condor pool and publish interesting metrics to a graphite time series database.

## duc

The duc dashboard displays historical results from duc scans on a filesystem.  A cron job is run on the system that stores all of the duc scans and publishes some results to an influx database.  It uses the timestamp inside of the duc scan when publishing to influx, so it does not need to be run at the same time as the scans, nor from the same host.

The influx database should be created before running the script:

```
$ influx -precision rfc3339
> CREATE DATABASE duc_accounting
```


```bash
#!/bin/sh

influxUrl="http://influx:8086"
ducUserDumpRoot="/ceph/diskusage/ducdb/user"
ducFSDumpRoot="/ceph/diskusage/ducdb/filesystem"

for ducdb in ${ducUserDumpRoot}/*.duc ; do
    ducdata=$(duc info -d $ducdb -b | awk 'NR==2 {split($6,a,"/"); print "accounting,username="a[3]",filesystem="a[2]" bytes="$5",files="$3",dirs="$4" "mktime(gensub(/-|:/, " ", "g", $1" "$2))}')
    curl -XPOST "${influxUrl}/write?db=duc_accounting&precision=s" --data-binary "$ducdata"

    ducdata=$(duc info -d $ducdb -b | awk 'NR==2 {split($6,a,"/"); print "accounting,username="a[3]",filesystem="a[2]" apparentbytes="$5" "mktime(gensub(/-|:/, " ", "g", $1" "$2))}')
    curl -XPOST "${influxUrl}/write?db=duc_accounting&precision=s" --data-binary "$ducdata"
done

for ducdb in ${ducFSDumpRoot}/*.duc ; do
    ducdata=$(duc info -d $ducdb -b | awk 'NR==2 {fs=gensub(/.*\/(.+)/, "\\1", $1);print "accounting,filesystem="fs" bytes="$5",files="$3",dirs="$4" "mktime(gensub(/-|:/, " ", "g", $1" "$2))}')
    curl -XPOST "${influxUrl}/write?db=duc_accounting&precision=s" --data-binary "$ducdata"

    ducdata=$(duc info -d $ducdb -a -b | awk 'NR==2 {fs=gensub(/.*\/(.+)/, "\\1", $1);print "accounting,filesystem="fs" apparentbytes="$5" "mktime(gensub(/-|:/, " ", "g", $1" "$2))}')
    curl -XPOST "${influxUrl}/write?db=duc_accounting&precision=s" --data-binary "$ducdata"
done
```

## fiberchannel

The fiberchannel dashboard displays traffic and error rates for each port of a set of QLogic fiberchannel switches.  The prometheus snmp_exporter collector is used by the prometheus data source to gather metrics from the switches.

QLogic switches do not expose custom port names via snmp, so some metric relabelling needs to be done in prometheus to assign friendly names to each port.  Ports with the name `unused` are omitted from the grafana plots.

The prometheus configuration looks like this:

```yaml
  - job_name: 'qlogic'
    scrape_interval:    60s
    scrape_timeout:     20s
    static_configs:
    - targets:
      - 'qlogic1'
    metrics_path: '/snmp'
    params:
      module: [fiberchannel]
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 10.13.5.16:9116  # The SNMP exporter's real hostname:port.
    metric_relabel_configs:
      - source_labels: [fcFeModuleName]
        target_label: switchName
        regex: 0x100000C0DD18FE13
        replacement: qlogic1
      - source_labels: [fcFeModuleName]
        target_label: switchName
        regex: 0x100000C0DD09987B
        replacement: qlogic5
[...]
# qlogic1 port names
      - source_labels: [fcFeModuleName,fcFxPortIndex]
        target_label: portName
        regex: 0x100000C0DD18FE13;1
        replacement: ibm-1:1a
      - source_labels: [fcFeModuleName,fcFxPortIndex]
        target_label: portName
        regex: 0x100000C0DD18FE13;2
        replacement: qlogic5
      - source_labels: [fcFeModuleName,fcFxPortIndex]
        target_label: portName
        regex: 0x100000C0DD18FE13;3
        replacement: lloarchive:1
[...]
# qlogic5 port names
      - source_labels: [fcFeModuleName,fcFxPortIndex]
        target_label: portName
        regex: 0x100000C0DD09987B;1
        replacement: unused
      - source_labels: [fcFeModuleName,fcFxPortIndex]
        target_label: portName
        regex: 0x100000C0DD09987B;2
        replacement: qlogic1
      - source_labels: [fcFeModuleName,fcFxPortIndex]
        target_label: portName
        regex: 0x100000C0DD09987B;3
        replacement: unused
      - source_labels: [fcFeModuleName,fcFxPortIndex]
        target_label: portName
        regex: 0x100000C0DD09987B;4
        replacement: unused
      - source_labels: [fcFeModuleName,fcFxPortIndex]
        target_label: portName
        regex: 0x100000C0DD09987B;5
        replacement: tapedrive2:1
      - source_labels: [fcFeModuleName,fcFxPortIndex]
        target_label: portName
        regex: 0x100000C0DD09987B;6
        replacement: tapedrive1:1
```

The `snmp_exporter` fragment for `generator.yml` requires additional MIB files, which we found in the observium source tree:

* QLOGIC-MIB.mib
* QLOGIC-PRODUCTS-MIB.mib
* QLOGIC-SMI.mib
* FIBRE-CHANNEL-FE-MIB
* FC-MGMT-MIB

The `generator.yml` fragment for the `snmp_exporter` looks like this:

```yaml
# QLogic Fiberchannel switches
  fiberchannel:
    walk:
      - fcFxPortID
      - fcFxPortC3InFrames
      - fcFxPortC3OutFrames
      - fcFxPortC3Discards
      - fcFxPortLinkResetIns
      - fcFxPortLinkResetOuts
    lookups:
      - source_indexes: [fcFeModuleIndex]
        lookup: fcFeModuleName
```

## host

The host dashboard displays aggregate statistics for an entire cluster, as well as interesting per-host statistics.  Hosts are grouped into `Jobs` in prometheusso that you can display aggregations across only interesting subsets of servers.

The prometheus service uses a `node_exporter` metrics collector running on each host to gather data.

The `prometheus.yml` snippet is pretty straightforward:

```yaml
  - job_name: 'miscnodes'
    static_configs:
    - targets: ['vsm-nfs1:9100']
    - targets: ['vsmarchive:9100']
    - targets: ['ss1:9100']
    - targets: ['ldas-condor:9100']
    - targets: ['ldas-gridmoni:9100']
```

## internal_frame_lag

The internal frame lag dashboard displays the delay as frame data makes its
way from CDS to LDAS and then on to CIT.  It will show the lag for raw, hoft, rds, and minute/second trend frame files.

For each frame data type, the following lags are calculated:

A custom script scans the frame directory and looks at the GPS identifier in the filename of the most recent frame file.  This is compared to the current time to determine the lag for getting data from CDS into LDAS.  For derived data types (eg RDS, aggregated hoft), this is the time delay since the last derived frame was generated.

The script also looks at the contents of the diskcache `frame_cache_dump` file
to determine the latest time available for each frame type.  This is compared to the current GPS time to determine the diskcache lag.

Another script looks at GPS identifer in the filenames of the most recent frame file in /ceph.  This is compared to the current GPS time to determine the clusterstage lag.

Finally, a script scans the gridftp logs for the last successful file transfer and compares the GPS timestamp in the file name to the current GPS time to determine the gridftp transfer lag.  This lag should match up with the CIT data lag plots described above.

Data is stored in an influx database.

## kafka

The kafka dashboard is an experiemental dashboard to display information about the kafka brokers at a site, such as the message and data rates.  This dashboard needs to be customized with the names of the brokers at each site.  It uses the prometheus data source to collect metrics directly from the kafka brokers.  The kafka brokers need to be configured to export data to prometheus using the following command line options to the JVM process:

```
-javaagent:/opt/kafka_2.12-2.0.0/libs/jmx_prometheus_javaagent-0.3.1.jar=7072:/etc/kafka/mirrormaker-cit-to-llo.yml kafka.tools.MirrorMaker
```

## koji

This dashboard displays data about the packages in each of the koji tags.

## pdu_ups

This dashboard displays the power consumption and ups capacities in the LDAS computer room.

The data for this dashboard comes from prometheus.  Prometheus uses the snmp_exporter and snmpd service running on the dcs-loghost system.  dcs-loghost acts as a snmp proxy to the UPS and PDU devices.

## prometheus

This dashboard displays performance metrics for the prometheus service.

## puppet

The puppet dashboard collects performance metrics from each running puppet server/compiler.

Enable the graphite reporter on the puppet servers by setting the following in `/etc/puppetlabs/pupetserver/conf.d/metrics.conf`:

```
    registries: {
        puppetserver: {
            # specify metrics to allow in addition to those in the default list
            #metrics-allowed: ["compiler.compile.production"]

            reporters: {
                # enable or disable JMX metrics reporter
                jmx: {
                    enabled: true
                }
                # enable or disable Graphite metrics reporter
                graphite: {
                    enabled: true
                }
            }
        }
    }
    # this section is used to configure settings for reporters that will send
    # the metrics to various destinations for external viewing
    reporters: {
        graphite: {
            # graphite host
            host: "graphite"
            # graphite metrics port
            port: 2003
            # how often to send metrics to graphite
            update-interval-seconds: 5
        }
    }
```

## smart

The SMART dashboard shows SMART monitoring information for all selected disks in the cluster.  Disks that show perfect health for the selected metric are omitted from the plots.

The data is collected by prometheus using the txtfile_exporter module in the node_exporter collector.  A cron job running on each hosts runs a [custom script](http://devel.dob.sk/collectd-scripts/) which invokes smartctl and writes the formatted results to `/etc/node_exporter/txtfiles.d/smartd.prom`.

## temp

This dashboard displays temperature readings at various points in the LDAS room.  Like the PDU/UPS dashboard, it uses snmp_exporter with the snmp proxy running on dcs-loghost.

## wan_traffic

This dashboard shows the aggregate and per-host traffic for the LDAS WAN connections.

## zfs

This dashboard shows read and write statistics for zfs filesystems.

It uses the txtfile_exporter module in the node_exporter collector.  An ioztat service runs on each zfs fileserver to report usage statistics to /etc/node_exporter/txtfiles.d/ioztat.prom.

## llo overview

The LLO overview dashboard is a collection of panels from other dashboards that provide an overview of the health of the entire datacenter and cluster.
